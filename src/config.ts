const env = process.env;

export const config = () => ({
  port: env.PORT,
  jwt: {
    secret: env.JWT_SECRET,
    tokens: {
      access: {
        type: 'access',
        expiresIn: env.ACCESS_TOKEN_EXPIRES_IN,
      },
      refresh: {
        type: 'refresh',
        expiresIn: env.REFRESH_TOKEN_EXPIRES_IN,
      },
    },
  },
  database: {
    type: env.DATABASE_DIALECT,
    host: env.DATABASE_HOST,
    port: env.DATABASE_PORT,
    username: env.DATABASE_USER,
    password: env.DATABASE_PASSWORD,
    database: env.DATABASE_NAME,
    entities: [
      __dirname + '/**/*.entity.{js,ts}',
      __dirname + '/**/entities/*.entity.{js,ts}',
    ],
    synchronize: env.DATABASE_NAME_SYNCHRONIZE,
  },
});
